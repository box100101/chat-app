import { Text, Pressable } from 'react-native';
import React from 'react';
import { bgColor, border, flex, font, padding } from '../../global-styles';

type ButtonProps = {
  title: string;
  onPress: () => void;
  isLoading?: boolean;
  styles?: object;
  titleStyles?: object;
};

const Button: React.FC<ButtonProps> = ({
  title,
  onPress,
  isLoading = false,
  styles,
  titleStyles,
}) => {
  return (
    <Pressable
      onPress={onPress}
      style={[
        padding.p_y(10),
        padding.p_x(30),
        bgColor.blue_rgba,
        border.rounded(10),
        styles,
      ]}>
      <Text style={[font.text_14, font.font_regular, titleStyles]}>
        {title}
      </Text>
    </Pressable>
  );
};

export default Button;
