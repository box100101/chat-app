import { View, Text, TextInput } from 'react-native';
import React from 'react';
import { Controller } from 'react-hook-form';
import { border, size } from '../../global-styles';

type InputProps = {
  control: any;
  name: string;
  placeholder: string;
  iconLeft?: JSX.Element;
  rules?: object;
  innerStyles?: object;
  outterStyles?: object;
};
const Input: React.FC<InputProps> = ({
  control,
  name,
  placeholder,
  iconLeft,
  rules,
  innerStyles,
  outterStyles,
}) => {
  return (
    <View
      style={[
        size.w_full,
        size.h(52),
        border.w_1,
        border.rounded(14),
        outterStyles,
      ]}>
      <Controller
        control={control}
        name={name}
        rules={rules}
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            placeholder={placeholder || ''}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
            style={[
              { padding: 10, borderRadius: 10 },
              size.h_full,
              size.w_full,
              innerStyles,
            ]}
          />
        )}
      />
    </View>
  );
};

export default Input;
