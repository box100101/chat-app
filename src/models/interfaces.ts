interface IResponse {
  payload: object;
  message: string;
}

export type { IResponse };
