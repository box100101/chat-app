// Function Component
type FCNoneProps = {};
type FCNavigationProps = { navigation: any };

// Object
function StyleObject<
  obj extends { [key: string]: object | ((arg: number) => object) }
>(arg: obj): obj {
  return arg;
}

function ContentObject<obj extends { [key: string]: string }>(arg: obj): obj {
  return arg;
}

export type { FCNoneProps, FCNavigationProps };
export { StyleObject, ContentObject };
