import { colors, fonts, borders } from './variables';
import { PAGE_SIZE } from './variables/pagination';

export { colors, fonts, borders, PAGE_SIZE };
