// @color
const PRIMARY: string = '#ff9f0a';
const PRIMARY_RGBA: string = 'rgba(255, 159, 10, 0.1)';
const WHITE: string = '#ffffff';
const WHITE_RGBA: string = 'rgba(255, 255, 255, 0.3)';
const BLACK: string = '#000000';
const BLACK_RGBA: string = 'rgba(0, 0, 0, 0.6)';
const RED: string = '#ff0000';
const RED_RGBA: string = 'rgba(255, 0, 0, 0.1)';
const GREEN: string = '#17d13c';
const GREEN_RGBA: string = 'rgba(23, 209, 60, 0.1)';
const BLUE: string = '#0084ff';
const BLUE_RGBA: string = 'rgba(0, 132, 255, 0.1)';
const PURPLE: string = '#904c9a';
const GRAY_01: string = '#999999';
const GRAY_02: string = '#888888';
const GRAY_03: string = '#777777';
const GRAY_04: string = '#666666';
const GRAY_05: string = '#555555';
const GRAY_06: string = '#fafafa';
const GRAY_RGBA: string = 'rgba(119, 119, 119, 0.1)';

export {
  PRIMARY,
  PRIMARY_RGBA,
  WHITE,
  WHITE_RGBA,
  BLACK,
  BLACK_RGBA,
  GRAY_01,
  GRAY_02,
  GRAY_03,
  GRAY_04,
  GRAY_05,
  GRAY_06,
  GRAY_RGBA,
  RED,
  RED_RGBA,
  GREEN,
  GREEN_RGBA,
  BLUE,
  BLUE_RGBA,
  PURPLE,
};
