// @font family
// ----------------------------------------------------------------

// @font size
const FONT_SIZE_TEXT_NAVIGATOR: number = 10;
const FONT_SIZE_00: number = 12;
const FONT_SIZE_01: number = 13;
const FONT_SIZE_02: number = 14;
const FONT_SIZE_03: number = 16;
const FONT_SIZE_04: number = 18;
// ----------------------------------------------------------------

// @font weight
const FONT_WEIGHT_01: any = '400';
const FONT_WEIGHT_02: any = '500';
const FONT_WEIGHT_03: any = '600';
const FONT_WEIGHT_04: any = '700';
// ----------------------------------------------------------------

export {
  FONT_SIZE_TEXT_NAVIGATOR,
  FONT_SIZE_00,
  FONT_SIZE_01,
  FONT_SIZE_02,
  FONT_SIZE_03,
  FONT_SIZE_04,
  FONT_WEIGHT_01,
  FONT_WEIGHT_02,
  FONT_WEIGHT_03,
  FONT_WEIGHT_04,
};
