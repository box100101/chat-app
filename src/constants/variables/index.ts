import * as colors from './colors';
import * as fonts from './fonts';
import * as borders from './borders';
import * as space from './space';
import STATUS from './status';

export { colors, fonts, borders, space, STATUS };
