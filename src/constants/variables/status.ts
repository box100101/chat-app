enum STATUS {
  SUCCESS = 'success',
  FAIL = 'failed',
}

export default STATUS;
