import { colors } from '../constants';
import { StyleObject } from '../models/types';

const color = StyleObject({
  primary: { color: colors.PRIMARY },
  white: {
    color: colors.WHITE,
  },
  black: { color: colors.BLACK },
  red: { color: colors.RED },
  green: { color: colors.GREEN },
  blue: { color: colors.BLUE },
  purple: { color: colors.PURPLE },
  gray_1: { color: colors.GRAY_01 },
  gray_2: { color: colors.GRAY_02 },
  gray_3: { color: colors.GRAY_03 },
  gray_4: { color: colors.GRAY_04 },
  gray_5: { color: colors.GRAY_05 },
  gray_6: { color: colors.GRAY_06 },
} as const);

export default color;
