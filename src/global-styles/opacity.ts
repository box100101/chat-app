import { StyleObject } from "../models/types";

const opacity = StyleObject({
  opacity_0: { opacity: 0 },
  opacity_10: { opacity: 0.1 },
  opacity_20: { opacity: 0.2 },
  opacity_30: { opacity: 0.3 },
  opacity_40: { opacity: 0.4 },
  opacity_50: { opacity: 0.5 },
  opacity_60: { opacity: 0.6 },
  opacity_70: { opacity: 0.7 },
  opacity_80: { opacity: 0.8 },
  opacity_90: { opacity: 0.9 },
  opacity_100: { opacity: 1 },
} as const);

export default opacity;