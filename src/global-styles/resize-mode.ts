import { StyleObject } from '../models/types';

const resizeMode = StyleObject({
  cover: { resizeMode: 'relative' },
  contain: { resizeMode: 'contain' },
  stretch: { resizeMode: 'stretch' },
  repeat: { resizeMode: 'repeat' },
  center: { resizeMode: 'center' },
} as const);

export default resizeMode;
