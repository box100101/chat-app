import { colors } from '../constants';
import { StyleObject } from '../models/types';

const alignSelf = StyleObject({
  center: { alignSelf: 'center' },
  left: { alignSelf: 'flex-start' },
  right: { alignSelf: 'flex-end' },
} as const);

export default alignSelf;
