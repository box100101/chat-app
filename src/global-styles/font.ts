import { StyleObject } from '../models/types';

const font = StyleObject({
  text_8: { fontSize: 8 },
  text_9: { fontSize: 9 },
  text_10: { fontSize: 10 },
  text_11: { fontSize: 11 },
  text_12: { fontSize: 12 },
  text_13: { fontSize: 13 },
  text_14: { fontSize: 14 },
  text_15: { fontSize: 15 },
  text_16: { fontSize: 16 },
  text_17: { fontSize: 17 },
  text_18: { fontSize: 18 },
  text_20: { fontSize: 20 },
  text_24: { fontSize: 24 },
  text_26: { fontSize: 26 },
  text_28: { fontSize: 28 },
  text_30: { fontSize: 30 },
  text_32: { fontSize: 32 },
  text_34: { fontSize: 34 },
  text_36: { fontSize: 36 },
  text_38: { fontSize: 38 },
  text_40: { fontSize: 40 },
  font_thin: { fontFamily: 'Poppins-Thin' }, //fontWeight: 100
  font_thin_i: { fontFamily: 'Poppins-ThinItalic' }, //fontWeight: 100
  font_extra_light: { fontFamily: 'Poppins-ExtraLight' }, //fontWeight: 200
  font_extra_light_i: { fontFamily: 'Poppins-ExtraLightItalic' }, //fontWeight: 200
  font_light: { fontFamily: 'Poppins-Light' }, //fontWeight: 300
  font_light_i: { fontFamily: 'Poppins-LightItalic' }, //fontWeight: 300
  font_regular: { fontFamily: 'Poppins-Regular' }, //fontWeight: 400
  font_medium: { fontFamily: 'Poppins-Medium' }, //fontWeight: 500
  font_medium_i: { fontFamily: 'Poppins-MediumItalic' }, //fontWeight: 500
  font_semi_bold: { fontFamily: 'Poppins-SemiBold' }, //fontWeight: 600
  font_semi_bold_i: { fontFamily: 'Poppins-SemiBoldItalic' }, //fontWeight: 600
  font_bold: { fontFamily: 'Poppins-Bold' }, //fontWeight: 700
  font_bold_i: { fontFamily: 'Poppins-BoldItalic' }, //fontWeight: 700
  font_extra_bold: { fontFamily: 'Poppins-ExtraBold' }, //fontWeight: 800
  font_extra_bold_i: { fontFamily: 'Poppins-ExtraBoldItalic' }, //fontWeight: 800
  font_black: { fontFamily: 'Poppins-Black' }, //fontWeight: 900
  font_black_i: { fontFamily: 'Poppins-BlackItalic' }, //fontWeight: 900
} as const);

export default font;
