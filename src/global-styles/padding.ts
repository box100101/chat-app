import { StyleObject } from '../models/types';

const padding = StyleObject({
  p: (arg) => {
    return { padding: arg };
  },
  p_y: (arg) => {
    return { paddingVertical: arg };
  },
  p_x: (arg) => {
    return { paddingHorizontal: arg };
  },
  p_l: (arg) => {
    return { paddingLeft: arg };
  },
  p_r: (arg) => {
    return { paddingRight: arg };
  },
  p_t: (arg) => {
    return { paddingTop: arg };
  },
  p_b: (arg) => {
    return { paddingBottom: arg };
  },
} as const);

export default padding;
