import { StyleObject } from '../models/types';

const text = StyleObject({
  text_left: { textAlign: 'left' },
  text_right: { textAlign: 'right' },
  text_center: { textAlign: 'center' },
  text_justify: { textAlign: 'justify' },
  transform: {
    upperCase: { textTransform: 'uppercase' },
    lowerCase: { textTransform: 'lowercase' },
    capitalize: { textTransform: 'capitalize' },
    none: { textTransform: 'none' },
  },
} as const);

export default text;
