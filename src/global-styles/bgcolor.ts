import { colors } from '../constants';
import { StyleObject } from '../models/types';

const bgColor = StyleObject({
  white: {
    backgroundColor: colors.WHITE,
  },
  white_rgba: { backgroundColor: colors.WHITE_RGBA },
  primary: { backgroundColor: colors.PRIMARY },
  primary_rgba: { backgroundColor: colors.PRIMARY_RGBA },
  black: { backgroundColor: colors.BLACK },
  red: { backgroundColor: colors.RED },
  black_rgba: { backgroundColor: colors.BLACK_RGBA },
  red_rgba: { backgroundColor: colors.RED_RGBA },
  green_rgba: { backgroundColor: colors.GREEN_RGBA },
  blue_rgba: { backgroundColor: colors.BLUE_RGBA },
  gray_rgba: { backgroundColor: colors.GRAY_RGBA },
  gray_1: { backgroundColor: colors.GRAY_01 },
  gray_2: { backgroundColor: colors.GRAY_02 },
  gray_3: { backgroundColor: colors.GRAY_03 },
  gray_4: { backgroundColor: colors.GRAY_04 },
  gray_5: { backgroundColor: colors.GRAY_05 },
  gray_6: { backgroundColor: colors.GRAY_06 },
  color: (arg: string) => {
    return { backgroundColor: arg };
  },
} as const);

export default bgColor;
