import { Dimensions } from 'react-native';
import { StyleObject } from '../models/types';

const size = StyleObject({
  height_device: { height: Dimensions.get('screen').height },
  width_device: { width: Dimensions.get('screen').width },
  h_full: { height: '100%' },
  w_full: { width: '100%' },
  h_auto: { height: 'auto' },
  w_auto: { width: 'auto' },
  w: (arg: string | number) => {
    return { width: arg };
  },
  h: (arg: string | number) => {
    return { height: arg };
  },
  w_max: (arg: string | number) => {
    return { maxWidth: arg };
  },
  h_max: (arg: string | number) => {
    return { maxHeight: arg };
  },
  w_min: (arg: string | number) => {
    return { minWidth: arg };
  },
  h_min: (arg: string | number) => {
    return { minHeight: arg };
  },
  square: (arg: any) => {
    return { height: arg, width: arg };
  },
});

export default size;
