import { StyleObject } from '../models/types';

const flex = StyleObject({
  flex_1: { flex: 1 },
  flex_row: { flexDirection: 'row' },
  flex_col: { flexDirection: 'column' },
  justify_center: { justifyContent: 'center' },
  justify_between: { justifyContent: 'space-between' },
  justify_around: { justifyContent: 'space-around' },
  justify_start: { justifyContent: 'flex-start' },
  justify_end: { justifyContent: 'flex-end' },
  items_center: { alignItems: 'center' },
  items_start: { alignItems: 'flex-start' },
  items_end: { alignItems: 'flex-end' },
  items_baseline: { alignItems: 'baseline' },
  flex_center: { justifyContent: 'center', alignItems: 'center' },
} as const);
export default flex;
