import { StyleObject } from '../models/types';

const position = StyleObject({
  relative: { position: 'relative' },
  absolute: { position: 'absolute' },
  zIndex: (arg) => {
    return { zIndex: arg };
  },
  top: (arg: string | number) => {
    return { top: arg };
  },
  bottom: (arg: string | number) => {
    return { bottom: arg };
  },
  left: (arg: string | number) => {
    return { left: arg };
  },
  right: (arg: string | number) => {
    return { right: arg };
  },
} as const);

export default position;
