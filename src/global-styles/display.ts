import { StyleObject } from '../models/types';

const display = StyleObject({
  block: { display: 'block' },
  flex: { display: 'flex' },
  none: { display: 'none' },
} as const);

export default display;
