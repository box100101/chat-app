import flex from './flex';
import display from './display';
import position from './position';
import border from './border';
import font from './font';
import text from './text';
import bgColor from './bgcolor';
import opacity from './opacity';
import size from './size';
import padding from './padding';
import margin from './margin';
import resizeMode from './resize-mode';
import alignSelf from './alignSelf';
import color from './color';

export {
  flex,
  display,
  position,
  border,
  font,
  text,
  bgColor,
  opacity,
  size,
  padding,
  margin,
  resizeMode,
  alignSelf,
  color,
};
