import { borders, colors } from '../constants';
import { StyleObject } from '../models/types';

const border = StyleObject({
  rounded_full: { borderRadius: 9999 },
  rounded_primary: { borderRadius: borders.BR_01 },
  rounded: (arg) => {
    return { borderRadius: arg };
  },
  box_shadow_primary: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 2.62,

    elevation: 2,
  },
  color: {
    primary: { borderColor: colors.PRIMARY },
    color: (arg: string) => {
      return { borderColor: arg };
    },
  },
  w_1: {
    borderWidth: 1,
  },
  w: (arg) => {
    return { borderWidth: arg };
  },
} as const);

export default border;
