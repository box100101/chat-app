import { StyleObject } from '../models/types';

const margin = StyleObject({
  m: (arg) => {
    return { margin: arg };
  },
  m_y: (arg) => {
    return { marginVertical: arg };
  },
  m_x: (arg) => {
    return { marginHorizontal: arg };
  },
  m_l: (arg) => {
    return { marginLeft: arg };
  },
  m_r: (arg) => {
    return { marginRight: arg };
  },
  m_t: (arg) => {
    return { marginTop: arg };
  },
  m_b: (arg) => {
    return { marginBottom: arg };
  },
} as const);

export default margin;
