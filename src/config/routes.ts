const HOME_SCREEN: string = 'Home sreen';
const SIGN_IN_SCREEN: string = 'Sign in screen';
const SIGN_UP_SCREEN: string = 'Sign up screen';

export { HOME_SCREEN, SIGN_IN_SCREEN, SIGN_UP_SCREEN };
