import { View, Text } from 'react-native';
import React from 'react';

type SocialButtonProps = {
  icon: JSX.Element;
};

const SocialButton: React.FC<SocialButtonProps> = ({ icon }) => {
  return (
    <View>
      <Text>SocialButton</Text>
    </View>
  );
};

export default SocialButton;
