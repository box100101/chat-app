import { View, Text, Image } from 'react-native';
import { color, flex, font, margin, size } from '../../global-styles';
import React from 'react';
import { Button, Input } from '../../components';
import { useForm } from 'react-hook-form';

const SignInScreen = () => {
  // hooks
  const { control } = useForm();
  // ********************************
  // arrow functions
  const handleLogin = () => {
    console.warn('Login');
  };
  // ********************************
  return (
    <View style={[flex.flex_1, flex.items_center, flex.justify_start]}>
      <Image
        style={[size.square(140), margin.m_t(80)]}
        source={require('../../../assets/images/screen.png')}
      />
      <Text
        style={[font.font_medium, font.text_30, color.black, margin.m_b(40)]}>
        Login
      </Text>
      <Input
        name="email"
        control={control}
        placeholder="Enter email"
        outterStyles={[size.w('80%'), margin.m_b(20)]}
      />
      <Input
        name="email"
        control={control}
        placeholder="Enter password"
        outterStyles={[size.w('80%')]}
      />
      <Button
        title={'Sign In'}
        onPress={handleLogin}
        styles={[margin.m_t(50)]}
      />
    </View>
  );
};

export default SignInScreen;
